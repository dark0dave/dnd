variable "LINODE_TOKEN" {
  type = string
}

variable "ROOT_PASS" {
  type = string
}

variable "domain" {
  type    = string
  default = "infinitednd.com"
}
