# Infinitednd

Builds infinitednd.com

# Setup remote state

```bash
terraform init \
  -backend-config="address=https://gitlab.com/api/v4/projects/20209760/terraform/state/dnd" \
  -backend-config="lock_address=https://gitlab.com/api/v4/projects/20209760/terraform/state/dnd/lock" \
  -backend-config="unlock_address=https://gitlab.com/api/v4/projects/20209760/terraform/state/dnd/lock" \
  -backend-config="username=$GITLAB_USER_NAME" \
  -backend-config="password=$GITLAB_PERSONAL_ACCESS_TOKEN" \
  -backend-config="lock_method=POST" \
  -backend-config="unlock_method=DELETE" \
  -backend-config="retry_wait_min=5"
```
