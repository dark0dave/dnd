terraform {
  backend "http" {
  }
}


provider "linode" {
  token       = var.LINODE_TOKEN
}

resource "linode_instance" "dnd-linode" {
  image     = "linode/debian10"
  label     = "dnd"
  group     = "dnd"
  region    = "eu-west"
  type      = "g6-nanode-1"
  root_pass = var.ROOT_PASS

  provisioner "file" {
    source      = "foundryvtt-0.6.5.zip"
    destination = "/foundryvtt.zip"
    connection {
      password = var.ROOT_PASS
      host     = self.ip_address
    }
  }

  provisioner "file" {
    source      = "script.sh"
    destination = "/tmp/script.sh"
    connection {
      password = var.ROOT_PASS
      host     = self.ip_address
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "/tmp/script.sh",
    ]
    connection {
      password = var.ROOT_PASS
      host     = self.ip_address
    }
  }
}

resource "linode_volume" "dnd-storage" {
  label     = "dnd-volume"
  region    = linode_instance.dnd-linode.region
  linode_id = linode_instance.dnd-linode.id
  size      = 25
  lifecycle {
    prevent_destroy = true
  }
}

resource "linode_domain" "domain" {
  type      = "master"
  domain    = var.domain
  soa_email = "hostmaster@registrar-servers.com"
  group     = "dnd"
  tags      = ["dnd"]
}

resource "linode_domain_record" "www" {
  domain_id   = linode_domain.domain.id
  name        = "www"
  record_type = "A"
  target      = linode_instance.dnd-linode.ip_address
}

resource "linode_domain_record" "www-ipv6" {
  domain_id   = linode_domain.domain.id
  name        = "www"
  record_type = "AAAA"
  target      = replace(linode_instance.dnd-linode.ipv6, "/64", "")
}

resource "linode_domain_record" "mail" {
  domain_id   = linode_domain.domain.id
  name        = "mail"
  record_type = "A"
  target      = linode_instance.dnd-linode.ip_address
}

resource "linode_domain_record" "mail-ipv6" {
  domain_id   = linode_domain.domain.id
  name        = "mail"
  record_type = "AAAA"
  target      = replace(linode_instance.dnd-linode.ipv6, "/64", "")
}

resource "linode_domain_record" "domain-record" {
  domain_id   = linode_domain.domain.id
  name        = var.domain
  record_type = "A"
  target      = linode_instance.dnd-linode.ip_address
}

resource "linode_domain_record" "domain-record-ipv6" {
  domain_id   = linode_domain.domain.id
  name        = var.domain
  record_type = "AAAA"
  target      = replace(linode_instance.dnd-linode.ipv6, "/64", "")
}
