#!/usr/bin/env bash

# load volume
mkdir /app
echo "/dev/disk/by-id/scsi-0Linode_Volume_dnd-volume /app ext4 defaults,noatime,nofail 0 2" >> /etc/fstab
mount -va

# Set host name for dns
hostnamectl set-hostname infinitednd.com
echo "127.0.0.1 infinitednd.com" >> /etc/hosts

# update all package sources
apt update -yqq
apt install apt-transport-https gnupg wget unzip nginx python-certbot-nginx -yqq

mkdir -p /app/foundrycore /app/foundrydata
cd /app/foundrycore

# Extract foundryvtt
cp /foundryvtt.zip /app/foundrycore/.
unzip foundryvtt.zip >/dev/null
rm foundryvtt.zip

# Install nodejs
wget -qO- https://deb.nodesource.com/setup_12.x | bash - >/dev/null
apt-get -qqy install nodejs

# Systemd
cat << EOF > /etc/systemd/system/foundry.service
[Unit]
Description=Foundry VTT
After=network.target

[Service]
User=root
ExecStart=node /app/foundrycore/resources/app/main.js  --dataPath=/app/foundrydata
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl start foundry.service
systemctl enable foundry.service

# nginx
cat << EOF > /etc/nginx/sites-available/foundry
server {
    listen 80;
    listen [::]:80;
    server_name infinitednd.com;

    location / {
        proxy_pass http://localhost:30000;
        proxy_intercept_errors on;
        proxy_set_header Host            \$host;
        proxy_set_header X-Real-IP       \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;

        # required for websockets to work
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_read_timeout      90;
    }
}
EOF

ln -s /etc/nginx/sites-available/foundry /etc/nginx/sites-enabled/foundry
rm /etc/nginx/sites-enabled/default
service nginx restart
